/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Ayushkumar
 */
public class MarketOfferCatalog {
    
    private ArrayList<MarketOffer> mktcatalog;

    public MarketOfferCatalog(){
    mktcatalog=new ArrayList<MarketOffer>();
    }    
    

    public ArrayList<MarketOffer> getMktcatalog() {
        return mktcatalog;
    }

    public void setMktcatalog(ArrayList<MarketOffer> mktcatalog) {
        this.mktcatalog = mktcatalog;
    }

    public MarketOffer addMarketOffer(){
       MarketOffer marketOffer = new MarketOffer();
       mktcatalog.add(marketOffer);
       return marketOffer;        
    }    
    
}
