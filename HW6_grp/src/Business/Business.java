/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Ayushkumar
 */
public class Business {
    
    private SupplierDirectory supplierDirectory;
    private MarketOfferCatalog marketOfferCatalog;
    private MarketList SupermktList;
    private UserDirectory userDir;
    
    public Business(){
    supplierDirectory = new SupplierDirectory();
    marketOfferCatalog = new MarketOfferCatalog();
    SupermktList = new MarketList();
    userDir = new UserDirectory();
    }

    public SupplierDirectory getSupplierDirectory() {
        return supplierDirectory;
    }

    public void setSupplierDirectory(SupplierDirectory supplierDirectory) {
        this.supplierDirectory = supplierDirectory;
    }

    public MarketOfferCatalog getMarketOfferCatalog() {
        return marketOfferCatalog;
    }

    public void setMarketOfferCatalog(MarketOfferCatalog marketOfferCatalog) {
        this.marketOfferCatalog = marketOfferCatalog;
    }

    public MarketList getSupermktList() {
        return SupermktList;
    }

    public void setSupermktList(MarketList SupermktList) {
        this.SupermktList = SupermktList;
    }

    public UserDirectory getUserDir() {
        return userDir;
    }

    public void setUserDir(UserDirectory userDir) {
        this.userDir = userDir;
    }

    
    
    
    
}
