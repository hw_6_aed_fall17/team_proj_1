/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import javax.swing.JOptionPane;

/**
 *
 * @author Yash
 */
public class ConfigData {

        public static Business Initialize(Business business){
            
        //Business business = new Business();
        UserDirectory userDir = business.getUserDir();
        User u1  = userDir.addUser();
        u1.setUsername("admin");
        u1.setPassword("123");
        u1.setRole("Administrator");
        
        User u2 = userDir.addUser();
        u2.setUsername("supplier1");
        u2.setPassword("123");
        u2.setRole("Supplier");
        
        User u3 = userDir.addUser();
        u3.setUsername("supplier2");
        u3.setPassword("123");
        u3.setRole("Supplier");
        
        User u4 = userDir.addUser();
        u4.setUsername("supplier3");
        u4.setPassword("123");
        u4.setRole("Supplier");
        
        User u5 = userDir.addUser();
        u5.setUsername("supplier4");
        u5.setPassword("123");
        u5.setRole("Supplier");
        
        User u6 = userDir.addUser();
        u6.setUsername("supplier5");
        u6.setPassword("123");
        u6.setRole("Supplier");
        
        User u7 = userDir.addUser();
        u7.setUsername("sales1");
        u7.setPassword("123");
        u7.setRole("Sales Person");
        
        User u8 = userDir.addUser();
        u8.setUsername("sales2");
        u8.setPassword("123");
        u8.setRole("Sales Person");
        
         User u9 = userDir.addUser();
        u9.setUsername("sales3");
        u9.setPassword("123");
        u9.setRole("Sales Person");
        
         User u10 = userDir.addUser();
        u10.setUsername("sales4");
        u10.setPassword("123");
        u10.setRole("Sales Person");
        
         User u11 = userDir.addUser();
        u11.setUsername("sales5");
        u11.setPassword("123");
        u11.setRole("Sales Person");
        
        business.setUserDir(userDir);
            
        
        Market m = business.getSupermktList().addMarket();

       m.setName("Northeastern");
       m.setTypeOfMarket("Education");
       
       Market m1 = business.getSupermktList().addMarket();
       m1.setName("Boston");
       m1.setTypeOfMarket("Education");
       
       Market m2 = business.getSupermktList().addMarket();
       m2.setName("BOFA");
       m2.setTypeOfMarket("Banking");
       
       Market m3 = business.getSupermktList().addMarket();
       m3.setName("Santander");
       m3.setTypeOfMarket("Banking");
       
       Supplier s=business.getSupplierDirectory().addSupplier();
       s.setSupplyName("Sony");
       
       Supplier s2=business.getSupplierDirectory().addSupplier();
       s2.setSupplyName("Samsung");

       Supplier s3=business.getSupplierDirectory().addSupplier();
       s3.setSupplyName("Toshiba");
       
       
       

       
       
//      Customer c=  m.addCustomer();
//       
//  c.setTypeOfMarket("Education");
//        c.setName("Northeastern");
//      
//        Customer c2=m.addCustomer();
//        c2.setName("BOFA");
//         c2.setTypeOfMarket("Banking");
//         
//         Customer c3=m.addCustomer();
//        c3.setName("Boston University");
//         c3.setTypeOfMarket("Education");
//         
//         Customer c4=m.addCustomer();
//        c4.setName("Santander");
//         c4.setTypeOfMarket("Banking");
         
       

      
        
        return business;
        
    }
    
}
