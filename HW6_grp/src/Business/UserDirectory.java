/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Yash
 */
public class UserDirectory {
    private ArrayList<User>userList;
    
    public UserDirectory()
    {
        userList = new ArrayList<User>();
    }

    public ArrayList<User> getUserList() {
        return userList;
    }

    public void setUserList(ArrayList<User> userList) {
        this.userList = userList;
    }
     public User addUser()
     {
     User u = new User();
     userList.add(u);
     return u;
     }
     
     public User isValidUser(String username,String password)
    {
        for(User user:userList)
        {
            if((user.getUsername().equals(username))&&(user.getPassword().equals(password)))
            return user;    
        }
        return null;
    }
}
